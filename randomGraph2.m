(* g = RandomGraph[{19, 28}, VertexLabels -> "Name", ImagePadding -> 10] *)
h = RandomGraph[DegreeGraphDistribution[{3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1}], VertexLabels -> "Name", ImagePadding -> 10]
While[!ConnectGraphQ[h], h = RandomGraph[DegreeGraphDistribution[{3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1}], VertexLabels -> "Name", ImagePadding -> 10]]
Export["h.png",h]
Print[EdgeList[h]]
Exit[]
