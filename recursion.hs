{-# OPTIONS_GHC -XFlexibleInstances #-}
{-# OPTIONS_GHC -XTypeSynonymInstances #-}
{-# OPTIONS_GHC -O2 #-}
{-# OPTIONS_GHC -rtsopts #-}

module Main where
import Data.List.Ordered(union,minus,nub)
import Data.List hiding(union,nub)
import Data.Set(Set)
import Data.Map(Map)
import qualified Data.Set as DS
import qualified Data.Map as DM
import Data.Sequence(Seq,(><), (<|), (|>))
import qualified Data.Sequence as S
import qualified Data.Foldable as F
import Control.Monad
import Control.Applicative
import Data.Function
import Control.Parallel.Strategies
import Data.Function.Memoize
import Data.MemoCombinators as Memo

type Probability = Double -> Double
type Result = (Int, Double)
type Expr = ((Int, Int), Probability)
type ExprList = [Expr]
type ResultList = [Result]
type Vertex = Int
type Graph = Map Vertex [Vertex]
-- that's silly as the final result is merely linear combination of a,b
--weighta = \(a,b) -> a
--weightb = \(a,b) -> b

prob :: Probability
prob = id

prob0 :: Probability
prob0 = return 0

emptyExpr :: Expr
emptyExpr = ((0,0), return 1)

lessThan0 :: (Int, Int) -> Bool
lessThan0 (x,y) = x <= 0 && y <= 0

weight::ExprList
weight = [((1,0),prob), ((0,1),(1-) . prob)]

myminus :: Expr -> Expr -> Expr
myminus ((x1,y1),p1) ((x2,y2),p2) = ((x1 - x2,y1 - y2), myproduct2 p1 p2)

myplus :: Expr -> Expr -> Expr
myplus  ((x1,y1),p1) ((x2,y2),p2) = ((x1+x2,y1+y2), myproduct2 p1 p2)

myplus2 :: Probability -> Probability -> Probability
myplus2 p1 p2 = (+) <$> p1 <*> p2

myproduct2 :: Probability -> Probability -> Probability
myproduct2 p1 p2 = (*) <$> p1 <*> p2

composeMinus :: ExprList -> ExprList
composeMinus x = groupClean $ myminus <$> weight <*> x

composePlus :: [ExprList] -> ExprList
composePlus [] = [emptyExpr]
composePlus x = foldl1 (\y z -> groupClean $ myplus <$> y <*> z) x

composePlus2 :: [Probability] -> Probability
composePlus2 [] = prob0
composePlus2 x = foldl1 myplus2 x

groupClean :: ExprList -> ExprList
groupClean = map (\xs@(x:_) -> (fst x, composePlus2 $ map snd xs)) . groupBy ((==) `on` fst) . sortBy (compare `on` fst) . map (\x -> if lessThan0 $fst x then ((0,0),snd x) else x)

groupClean2 :: ResultList -> ResultList
groupClean2 = map (\xs@(x:_) -> (fst x, sum $ map snd xs)) . groupBy ((==) `on` fst) . sortBy (compare `on` fst) . map (\x -> if 0 > fst x then (0, snd x) else x)

recurse' :: Integer -> Vertex -> Graph -> ExprList
recurse' 0 _ _ = [emptyExpr]
recurse' r u g = recurseHelper recurse' r u g

recurse :: Integer -> Vertex -> Graph -> ExprList
recurse 0 _ _ = weight
recurse r u g = recurseHelper memoRecurse r u g

memoRecurse r u g = (memoize3 (\a b c -> recurse a b (read c))) r u (show g)
--memoRecurse r u g = (Memo.memo3 Memo.integral Memo.integral memoString (\a b c -> recurse a b (read c))) r u (show g) where
	memoString = Memo.list Memo.char

recurseHelper :: (Integer -> Vertex -> Graph -> ExprList) -> Integer -> Vertex -> Graph -> ExprList
recurseHelper recf r u g = if isolated u g then [emptyExpr] else 
				(composeMinus $ composePlus [recf (r-1) v (subgraph g u l) | (v,l) <- zip ((DM.!) g u) [1..] ] ) 
					--`using` parListChunk (max 2 (div 10 $ fromInteger r)) rdeepseq 

isolated :: Vertex -> Graph -> Bool
isolated v g = case DM.lookup v g of
		       Just x -> null x
		       Nothing -> True

subgraph :: Graph -> Vertex -> Integer -> Graph
subgraph g u l = DM.map (filter (`notElem` excludeSet)) $ DM.filterWithKey (\x _ -> (x `notElem` excludeSet)) g where
			excludeSet = u:take (fromInteger l-1) ((DM.!) g u)

fromList :: [(Vertex, Vertex)] -> Graph
fromList s =  DM.fromList $ map (\(xs@((a,_):_)) -> (a, map snd xs)) $ groupBy ((==) `on` fst) $sort $s ++ map (\(a,b)->(b,a)) s

eval :: Int -> Int -> Double -> Expr -> Result
eval a b q ((x,y),p) = (a*x + b*y, p q)

showProb :: Probability -> String
showProb p = "Probability {" ++ show [p 0, p 0.25, p 0.333, p 0.5, p 0.666, p 0.75, p 1] ++ "}"

showExprList :: ExprList -> String
showExprList l = "ExprList {" ++ (unlines $ map show l) ++ "}"

showResult :: ResultList -> String
showResult l = "ResultList {" ++ (unlines $ map show l) ++ "}"

--TODO maybe integrate a,b,p,r into the results
showIteration l = "Iteration {" ++ (unlines $ zipWith (\v s -> "\r\nVertex " ++ show v ++ ":\r\n" ++ s) [1..] $ map showResult l) ++ "}"

instance Show Probability where show = showProb

instance Show ExprList where show = showExprList

testgl :: [(Vertex, Vertex)]
testgl = [ (1,9), (2,6), (2,13), (2,16), (3,10), (4,8), (4,9), (4,12), (5,13), (6,8), (6,11), (6,12), (6,18), (7,10), (7,13), (8,17), (9,13), (9,14), (9,15), (10,11), (13,15), (14,15), (14,16), (14,17), (14,18), (15,16), (15,19), (16,19) ]

main = interact (unlines . map showIteration . test . lines ) 

test [] = []
test (sg:sa:sb:sp:sr:xs) = let g = fromList $ read sg
			       a = read sa
			       b = read sb
			       p = read sp
			       r = read sr in 
			       [ (groupClean2 $ map (eval a b p) $ memoRecurse r u g) `using` parListChunk 2 rdeepseq 
				  | u <- [1..fst $ DM.findMax g]] : test xs
