Assumption
==============
* You have eog.
* You have ghc (better be ghc 7.6) and corresponding packages.
* You have Mathematica (I know this might be a hard one, so feel free to replace the graph generation with your own suite).

Build
=============

        make recursion
        
Run
==============
Test with

        ./randomGraph.sh >> a
        (* put on the glasses and examine the graph shown up, next is to build up test suite with suitable parameters *)
        vim a
        ./recursion +RTS -N -s -l < a
        
Observation
=================
* When degree is large enough, it gets pretty unstable at that vertex.
* For a path, or any vertex that cuts the graph into several connected components, it's likely that that node should be picked first.
* For exponential distribution, one good thing is that one could recursively pick some nodes with really small weight, and the rest maintains exponential distribution.

Misc
===========
* so strange that memoization doesn't seem to have much effects
