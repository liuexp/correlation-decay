module Main where
--import Data.List.Ordered(union,minus,nub)
import Data.List hiding(union,nub)
import Data.Set(Set)
import Data.Map(Map)
import qualified Data.Set as DS
import qualified Data.Map as DM
import Data.Sequence(Seq,(><), (<|), (|>))
import qualified Data.Sequence as S
import qualified Data.Foldable as F
import Control.Monad
import Control.Applicative
import Data.Function
import Control.Parallel.Strategies

-- graph g is V-> [V]
--subgraph :: Map Integer [Integer] -> Integer -> Integer -> Map Integer [Integer]
subgraph g u l = DM.map (filter (`notElem` excludeSet)) $ DM.filterWithKey (\x _ -> (x `notElem` excludeSet)) g where
			excludeSet = u:take (fromInteger l-1) ((DM.!) g u)

--weight :: Map Integer b
weight u = 1

--bonus :: Integer -> Integer -> Map [Integer] Integer -> b
bonus 0 u g = weight u
bonus r u g = max 0 $ weight u - sum [bonus (r-1) v (subgraph g u l)| (v,l) <- zip ((DM.!) g u) [1..]]

bonus' 0 u g = 0
bonus' r u g = max 0 $ weight u - sum [bonus' (r-1) v (subgraph g u l)| (v,l) <- zip ((DM.!) g u) [1..]]

fromList s =  DM.fromList $ map (\(xs@((a,b):_)) -> (a, map snd xs)) $ groupBy ((==) `on` fst) $sort $s ++ map (\(a,b)->(b,a)) s

test gl = putStrLn $ unlines $ [show (i,l, sum $ map (\(a,b)-> max a b) l, sum $map (\(a,b)->min a b) l) | (l,i) <- zip testset [1..]] where
	g = fromList gl
	n = DM.size g
	testset = map (\r -> map (\u ->(bonus r u g, bonus' r u g)) [1..n]) [1..n]

testgl = [ (1,9), (2,6), (2,13), (2,16), (3,10), (4,8), (4,9), (4,12), (5,13), (6,8), (6,11), (6,12), (6,18), (7,10), (7,13), (8,17), (9,13), (9,14), (9,15), (10,11), (13,15), (14,15), (14,16), (14,17), (14,18), (15,16), (15,19), (16,19) ]
